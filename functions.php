<?php
/**
 * GreatLife functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GreatLife
 */

if ( ! function_exists( 'greatlife_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function greatlife_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on GreatLife, use a find and replace
	 * to change 'greatlife' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'greatlife', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'greatlife' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'greatlife_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	
	// Add theme support for post formats
	add_theme_support('post-formats', array('video'));
}
endif;
add_action( 'after_setup_theme', 'greatlife_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function greatlife_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'greatlife_content_width', 640 );
}
add_action( 'after_setup_theme', 'greatlife_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function greatlife_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'greatlife' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'greatlife' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets', 'greatlife' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'greatlife' ),
		'before_widget' => '<div class="col-md-3 footer-widget"><section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section></div>',
		'before_title'  => '<h3 class="footer-widget-title">',
		'after_title'   => '</h3>',
	) );
	
}

add_action( 'widgets_init', 'greatlife_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function greatlife_scripts() {
	
	wp_enqueue_style( 'greatlife-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'gl_bootstrap-grid', get_template_directory_uri() . '/assets/css/bootstrap-grid.css' );
	
	wp_enqueue_style( 'gl_style', get_template_directory_uri() . '/assets/css/gl_style.css' );
	
	wp_enqueue_style( 'gl_responsive', get_template_directory_uri() . '/assets/css/gl_responsive.css' );
	
	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.min.js', array('jquery'), '1.12.1', true );
	
	wp_enqueue_script( 'gl_script', get_template_directory_uri() . '/assets/js/gl_script.js', array('jquery'), '1.0', true );
	
	if (is_home() || is_front_page()) {
		wp_enqueue_style( 'slick-style', 'https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );
		wp_enqueue_script('slick-js', 'https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js' );
		wp_enqueue_script('gl_slider-js', get_template_directory_uri() . '/assets/js/gl_slider.js', array('jquery', 'slick-js'), '1.0', true);
	}
	
	if( is_page_template('gallery.php') ) {
		wp_enqueue_script( 'gl_gallery', get_template_directory_uri() . '/assets/js/gl_gallery.js', array('jquery'), '1.0', true );
	}
	if( is_page('contact') ) {
		wp_enqueue_script( 'gl_map', get_template_directory_uri() . '/assets/js/gl_map.js', array('jquery', 'google-maps-api'), null, false );
		wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD4_fmBeV4_WTDn0rgZrsFl1kZpmjXlm48', null, false );
	}
	
	wp_enqueue_script( 'greatlife-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'greatlife_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Helper function for dynamically adding list items to a given menu
 * In this theme it is used to add a list item to the main menu that is used to create a trailing hover effect
 */
function _custom_nav_menu_item( $title, $url, $order, $parent = 0 ){
  $item = new stdClass();
  $item->ID = 1000000 + $order + $parent;
  $item->db_id = $item->ID;
  $item->title = $title;
  $item->url = $url;
  $item->menu_order = $order;
  $item->menu_item_parent = $parent;
  $item->type = '';
  $item->object = '';
  $item->object_id = '';
  $item->classes = array();
  $item->target = '';
  $item->attr_title = '';
  $item->description = '';
  $item->xfn = '';
  $item->status = '';
  return $item;
}

add_filter( 'wp_get_nav_menu_items', 'custom_nav_menu_items', 20, 2 );

function custom_nav_menu_items( $items, $menu ){

	  $items[] = _custom_nav_menu_item('', '#', 11);
	  return $items;

}

/* Add a custom thumbnail size for archive and blog pages, cropping from top center */

add_image_size( 'blog-thumbnail', 220, 240, array( 'center', 'top' ) );
add_image_size( 'gallery-thumbnail', 400, 400, array( 'center', 'top' ) );

/* If Advanced Custom Fields plugin is installed, add an options page for theme options */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Media Links',
		'menu_title'	=> 'Social Media Links',
		'parent_slug'	=> 'theme-settings',
	));

}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

