<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GreatLife
 */

?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="bs_container">	
		<div class="row footer-widgets">
			<?php dynamic_sidebar('sidebar-2'); ?>
			<div class="col-md-3 footer-widget">
				<h3>Connect</h3>
				<?php get_template_part( 'template-parts/content', 'social-links' ); ?>
			</div>
			<div id="footer-widgets_underlay"></div>
		</div>
	</div>
	<div class="site-info bs_container-fluid">
		<p id="copyright">&copy; <?php echo date('Y'); ?> Great Life Chiropractic. All Rights Reserved. | <a style="color: #ccc; text-decoration: none;" target="_blank" href="http://www.stolen-sheep.com">Site Design</a></p>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
