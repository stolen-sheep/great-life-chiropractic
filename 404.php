<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package GreatLife
 */

get_header(); ?>

	<div id="primary" class="content-area container-fluid">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">

				<div class="page-content">
				
					<div id="error404">
						<h2>We're sorry, but it doesn't look like that page exists.</h2>
						<p>Maybe you'd like to try to search for what you're looking for:</p>
						<?php get_search_form(); ?>
						<p>or just <a href="<?php echo get_home_url(); ?>">head back to the site?</a></p>
					</div>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
