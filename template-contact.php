<?php /*
* Template Name: Contact Page
*/

get_header();
get_template_part( 'template-parts/content', 'image-header' );
get_template_part( 'template-parts/content', 'cta' ); ?>

<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php
					while ( have_posts() ) : the_post();
		
						get_template_part( 'template-parts/content', 'page' );
		
					endwhile; // End of the loop.
				?>
	
	</main><!-- #main -->
</div><!-- #primary -->
<?php get_template_part( 'template-parts/content', 'map' );
get_footer();
