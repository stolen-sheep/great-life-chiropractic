<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package GreatLife
 */

get_header(); ?>

	<div class="bs_container">
		<div class="row">
			<div id="primary" class="content-area col-md-9">
				<main id="main" class="site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();
			
						get_template_part( 'template-parts/content', get_post_format() );
			
					endwhile; // End of the loop.
					?>

				</main><!-- #main -->
			</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
