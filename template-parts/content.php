<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GreatLife
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('bs_container'); ?>>
	
<?php the_category(); ?>

<div class="row">
	
<?php if ( has_post_thumbnail() ) : ?>

<header class="entry-header col-md-4">
    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        	<?php the_post_thumbnail('blog-thumbnail'); ?>
    	</a>
</header><!-- .entry-header -->

<div class="entry-content col-md-8">

<?php else : ?>

<div class="entry-content col-md-12">
	
<?php endif; ?>
	
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>
		
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'greatlife' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'greatlife' ),
				'after'  => '</div>',
			) );
		?>
</div><!-- .entry-content -->

</div>
</article><!-- #post-## -->
