<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GreatLife
 */

?>

<?php if( function_exists('have_rows') && have_rows('the_content') ) :
	
		$bg_color   = get_field('bg_color');
		$text_color = '#3f3e3e';
		
		$dark_bg = array('#3f3e3e', '#4d4d59', '#2c8ce6');
		
		if ( in_array( $bg_color, $dark_bg ) ) :
		
		$text_color = '#eeeeee';
		
		elseif ($bg_color == 'custom') :
			$bg_color = get_field('custom_bg');
			$text_color = get_field('text_color');
		
		endif; ?>
		
		<div class="content_bg" style="background-color: <?php echo $bg_color . ';'; ?> color: <?php echo $text_color . '!important;' ?>">
		
		<div class="bs_container">
			<div class="row">
		
		<?php while( have_rows('the_content') ) : the_row();
			
			$columns = get_sub_field('columns');
		
			if( $columns == '1' ) : 
			
				$column_1 = get_sub_field('column_1'); ?>
			
				<div class="col-xs-12">
				
					<?php echo $column_1; ?>
					
				</div>
			
			<?php elseif( $columns == '2' ) : 
				
					$column_1 = get_sub_field('column_1');
					$column_2 = get_sub_field('column_2');
				
			?>
			
				<div class="col-md-6">
				
					<?php echo $column_1; ?>
					
				</div>
				
				<div class="col-md-6">
				
					<?php echo $column_2; ?>
					
				</div>
					  		
			
			<?php endif; ?>
			
			</div>
		</div>
		</div>
			
		<?php endwhile;
		
	  endif;
?>
	
	
