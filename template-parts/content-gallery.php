<?php // the markup for gallery template pages ?>

<main id="main" class="site-main" role="main">

			<?php // get_template_part( 'template-parts/content', 'color-bars' ); ?>

			<?php
			while ( have_posts() ) : the_post(); ?>

				<div class="bs_container">

					<div class="row">

						<div class="col-md-12">

							<?php the_content(); ?>

						</div>

					</div>

				</div>

			<?php endwhile; // End of the main loop.

			wp_reset_postdata(); ?>


</main><!-- #main -->

<?php if ( function_exists('get_field') && get_field('gallery_category') ) :

	$gallery_cat = get_field('gallery_category') =='' ? 6 : get_field('gallery_category');

	$gallery_args = array(
		'cat' 		 => $gallery_cat,
		'meta_key' => '_custom_post_order',
	  'orderby' => 'meta_value date',
		'order' => 'ASC'
	);

	$query = new WP_Query( $gallery_args );

		if( $query->have_posts() ) :

		$oddEven = 'odd';
		$i		 = 1;

		?>
		<i id="gallery-modal_prev" class="fa fa-chevron-left fa-2x gallery-modal_arrow"></i>
		<div id="gallery-modal">

			<div id="gallery-frame">
				<div id="gallery-full"></div>
			</div>

		</div>
		<i id="gallery-modal_next" class="fa fa-chevron-right fa-2x gallery-modal_arrow"></i>

		<div class="bs_container-fluid gallery">
			<div class="row gallery-row">

			<?php while( $query->have_posts() ) : $query->the_post(); ?>

			<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

				  <div class="gallery-item col-xs-12 col-sm-6 col-md-3 <?php echo $oddEven; ?>" data-index="<?php echo $i; ?>">

					  <div style="background: url(<?php echo "'" . $url . "'" ?>); background-size: cover;" class="gallery-item-thumb"></div>

				  </div>

				  <div class="gallery-item-details">
						  <h2 class="gallery-item-title"><?php the_title(); ?></h2>
							<p class="gallery-item-excerpt"><?php the_excerpt(); ?></p>
				  </div>

			<?php

				if ( $oddEven == 'odd' ) { $oddEven = 'even'; } else {
					$oddEven = 'odd';
				}
				$i++;
			?>

			<?php endwhile; //endwhile wp_query ?>

			</div> <!-- .row -->
		</div> <!--end .bs_container -->

		<?php endif; //endif wp_query ?>

<?php endif; //endif get_field('gallery_category') ?>
