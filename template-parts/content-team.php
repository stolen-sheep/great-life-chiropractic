<?php 
		$sm_cols = '';
		$md_cols = '';
		$num_rows = '';
	
		// retrieve color selection
		$background_color = get_field('background_color');
		
		// if "custom" was selected, get chosen hex value
		if ($background_color == 'custom') {
			$background_color = get_field('color_code');
		}
		
		// how many 'rows'?
		if( have_rows('team_member') ) {
			$num_rows = count( get_field('team_member') );
		}
		
		// adjust column number accordingly
		if ($num_rows == 1) {
			
			$sm_cols = 12;
			$md_cols = 12;
			
		} else if ( $num_rows == 2 ) {
			
			$sm_cols = 6;
			$md_cols = 6;
			
		} else if ( $num_rows == 3 ) {
			
			$sm_cols = 12;
			$md_cols = 4;
			
		} else if ( $num_rows == 4 ) { 
			
			$sm_cols = 6;
			$md_cols = 3;
			
		} else {}
		
?>

<div id="team" style="background-color: <?php echo $background_color . ';'; ?>">
			
			<?php if( have_rows('team_member') ) : ?>
				
				<div class="bs_container">
					<div class="row">
					
					<?php while ( have_rows('team_member') ) : the_row(); ?>
					
						<?php 
							$sm_col_class = 'col-sm-' . $sm_cols; 
							$md_col_class = 'col-md-' . $md_cols;
						?>
					
						<div class="team_member col-xs-12 <?php echo $sm_col_class . ' ' . $md_col_class; ?>">
						
							
						
				<?php
					
					$portrait = get_sub_field('portrait');
					$url	  = $portrait['url'];
					$name 	  = get_sub_field('name');
					$title 	  = get_sub_field('title');
					$bio 	  = get_sub_field('bio'); ?>
					
					<div class="team_portrait-container" style="background: url(<?php echo "'" . $url . "'"; ?>);">
<!-- 						<img src="<?php echo $portrait['url']; ?>" alt="<?php echo $portrait['alt']; ?>" />	 -->
					</div>
					
					<?php 
					
					echo '<h2 class="team_member-name">' . $name . '</h2>';
					echo '<h4 class="team_member-title">' . $title . '</h4>';
					echo '<p class="team_member-bio">' . $bio . '</p>';
					
					echo '</div>';
					
					endwhile; ?>
					
					</div>
				</div>
			
			<?php endif; ?>
			
		</div>	
	</div>
</div>