<?php
/**
 * The template for closing the main content of the page.
 *
 * Contains the closing of the #content div, #page div.
 *
 * Because full-width template parts can be added in dynamically (via ACF), they must be inserted after the main content but before the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GreatLife
 */

?>

	</div><!-- #content -->

</div><!-- #page -->