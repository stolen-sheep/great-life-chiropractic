<?php if( function_exists('get_field') && get_field('facebook_link', 'option') ) :
	
		$facebook_link    = get_field('facebook_link', 'option');
		$facebook_icon 	  = get_field('facebook_icon', 'option');
		$google_plus_link = get_field('google_plus_link', 'option');
		$google_plus_icon = get_field('google_plus_icon', 'option');
		$youtube_link 	  = get_field('youtube_link', 'option');
		$youtube_icon 	  = get_field('youtube_icon', 'option');
		$yelp_link		  = get_field('yelp_link', 'option');
		$yelp_icon		  = get_field('yelp_icon', 'option');
		
?>

<ul id="social-links">
	<li class="social-link">
		<a href="<?php echo $facebook_link; ?>" target="_blank" alt="Great Life Chiropractic on Facebook">
			<i class="fa <?php echo 'fa-' . $facebook_icon; ?> "></i>Facebook
		</a>
	</li>
	<li class="social-link">
		<a href="<?php echo $youtube_link; ?>" target="_blank" alt="Great Life Chiropractic on YouTube">
			<i class="fa <?php echo 'fa-' . $youtube_icon; ?> "></i>YouTube
		</a>
	</li>
	<li class="social-link">
		<a href="<?php echo $google_plus_link; ?>" target="_blank" alt="Great Life Chiropractic on Google Plus">
			<i class="fa <?php echo 'fa-' . $google_plus_icon; ?> "></i>Google +
		</a>
	</li>
	<li class="social-link">
		<a href="<?php echo $yelp_link; ?>" target="_blank" alt="Great Life Chiropractic on Yelp!">
			<i class="fa <?php echo 'fa-' . $yelp_icon; ?> "></i>Yelp!
		</a>
	</li>
	<li class="social-link">
		<a href="https://clients.mindbodyonline.com/ASP/main_appts.asp?studioid=27944&tg=&vt=&lvl=&stype=&view=&trn=0&page=&catid=&prodid=&date=1%2f28%2f2017&classid=0&prodGroupId=&sSU=&optForwardingLink=&qParam=&justloggedin=&nLgIn=&pMode=0&loc=1" target="_blank" alt="Great Life Chiropractic Online Scheduling">
			<i class="fa fa-calendar "></i>Schedule Online
		</a>
	</li>
</ul>

<?php endif; ?>