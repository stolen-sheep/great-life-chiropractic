<div id="cta">
	<div class="bs_container">

		<?php
			$cta_title 	 	  = get_field('cta_title', 'option');
			$cta_copy  		  = get_field('cta_copy', 'option');
			$cta_schedule	  = get_field('cta_schedule_link', 'option');
			$cta_massage	  = get_field('cta_massage_schedule_link', 'option');
			$cta_phone		  = get_field('cta_phone', 'option');
			$schedule_link  = get_site_url() . '/online-schedule';

			// if (is_page('Massage Therapy')) :
			// 	$schedule_link = $cta_massage;
			// else :
			// 	$schedule_link = $cta_schedule;
			// endif;
		?>

		<div class="row">

			<div class="col-md-12">
				<h2><?php echo $cta_title; ?></h2>
				<p><?php echo $cta_copy; ?></p>
			</div>

			<div class="cta_button col-md-4">
				<a href="<?php echo $schedule_link; ?>">
					<button>Schedule</button>
				</a>
			</div>
			<div class="cta_button col-md-4">
				<a href="<?php echo $cta_phone; ?>">
					<button>Call</button>
				</a>
			</div>
			<div class="cta_button col-md-4">
				<a href="#" class="hidden-xs-down">
					<button>Newsletter</button>
				</a>
				<a id="sms_link" href="#" class="hidden-sm-up">
					<button>Tap to Text</button>
				</a>
			</div>
		</div>

	</div>
</div>
