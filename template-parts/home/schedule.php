<?php
	$schedule_1	  = get_field('cta_schedule_link', 'option');
	$schedule_2		= get_field('cta_massage_schedule_link', 'option');
?>

<div class="schedule-block-container">

	<hr class="schedule-block_hr">
	<h3>Schedule<br><span>an / a</span></h3>
	<hr class="schedule-block_hr">

	<div id="schedule-block-1" class="schedule-block">
		<a href="<?php echo $schedule_1; ?>" target="_blank">
 			<button class="schedule-block_button">Appointment</button>
		</a>
	</div>
	<div id="schedule-block_center">
		<div id="schedule-block_icon">
			<i class="fa fa-calendar fa-2x"></i>
		</div>
	</div>
	<div id="schedule-block-2" class="schedule-block">
		<a href="<?php echo $schedule_2; ?>" target="_blank">
			<button class="schedule-block_button">Massage</button>
		</a>
	</div>
</div>
