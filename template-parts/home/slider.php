<?php if( function_exists('get_field') && have_rows('slider', 'option') ) : ?>
	
	<div id="slider">
	
		<?php while( have_rows('slider', 'option') ) : the_row(); ?>
		
			<?php 
				
				$slide 	   = get_sub_field('slide', 'option'); 
				$slide_url = $slide['url'];							?>
			
				<div class="slide">
					<div class="slide-img" style="background-size: cover!important; <?php echo 'background: url(\'' . $slide_url . '\');'; ?> "></div>
				</div>
				
		<?php endwhile; ?>
		
	</div>
		
		
<?php endif; ?>