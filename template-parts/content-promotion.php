<div id="promotion">

	<div class="container-fluid promotion_description">

		<div id="promoBranding">
			<img id="promoLogoMark" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png">
			<h2 id="promoLogoText">Great Life</h2>
			<h3 id="promoLogoTag">Chiropractic &amp; Massage</h3>
		</div>

		<p id="promoMessage" class="promoDetails">It couldn't be simpler: sign up below and receive $20 off your first massage.</p>


	</div>

	<!---->

	<div class="container-fluid promotion_cta">

		<!-- <p id="promoDisclaimer" class="promoDetails">We promise, we'll only send occasional content, and you can opt out at any time.</p> -->

		<div class="promotion_form">

			<?php echo do_shortcode('[ctct form="272"]'); ?>

		</div>

	</div>

</div>
