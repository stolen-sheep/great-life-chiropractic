jQuery(document).ready(function($){
	
	// variables
	
	var win 		 = $(window);
	var winW		 = $(window).width();
	var body		 = $("body");
	var galleryItem  = $(".gallery-item");
	var postCount	 = galleryItem.length;
	var $dataIndex;
	var galleryImage;
	var myDetails;
	var galleryModal = $("#gallery-modal");
	var galleryFull  = $("#gallery-full");
	var container	 = $('.gallery-row');
	var galPrev		 = $("#gallery-modal_prev");
	var galNext		 = $("#gallery-modal_next");
	var galArrows	 = $(".gallery-modal_arrow");
	
	// add "js-enabled" class to body for javascript detection
	
	body.addClass("js-enabled");
	
	// update window width var on resize
	
	win.on('resize', function(){ winW = win.width(); });
	
	// gallery detail hover toggle
	
	galleryItem.mouseenter(function(){
		
		var $this = $(this);
		myDetails = $this.next('.gallery-item-details');
		
		var galleryItemWidth  = $this.width();
		var galleryItemHeight = $this.height();
		
		if (winW < 575) {
			
				var latPos = $this.position().left;
				var lonPos = $this.position().top + $this.height();
				
		} else {
			
			var lonPos = $this.position().top;
			
			// put odd # detail divs to the right, even to the left
			
				if( $this.hasClass('odd') ) {
					var latPos = $this.position().left + $this.width();
				} else {
					var latPos = $this.position().left - $this.width();
				}
			
		}
	
 		myDetails.css({
	 		'width'  : galleryItemWidth,
	 		'height' : galleryItemHeight,
	 		'left'	 : latPos,
	 		'top'	 : lonPos,
 		}).fadeIn('fast');

	}).mouseleave(function(){
		
		var $this = $(this);
		myDetails = $this.next('.gallery-item-details');
		
		myDetails.stop().fadeOut();
		
	});
	
	// when a thumbnail is clicked, fade the modal in, add associated details to info window, and add its background image to the modal display
	
	galleryItem.on('click', function(){
		
		var $this = $(this);
		$dataIndex = $(this).data("index");
		galleryImage = $this.find('.gallery-item-thumb').css("background-image");
		galleryModal.fadeIn('fast').addClass('modal-active');
		galArrows.fadeIn('fast'); 
		galleryFull.css({ 'background-image' : galleryImage });
		
	});
	
	// if the user clicks on the dark area of the modal window, fade out
	
	galleryModal.on('click', function(){
		galleryModal.fadeOut('slow').removeClass('modal-active');
		galArrows.fadeOut('slow');
	});
	
	// when a key is pressed, if the modal window is open, close if it is escape (27), advance to the next image if it is the right arrow (39), revert to the previous if it is the left arrow (37)
	
	$(document).on('keyup', function(e) {
		
    	var key = e.which || e.keyCode;
    	
    	if(galleryModal.hasClass('modal-active')) {
    	    	
			switch(key) {
				case 27:
					galleryModal.fadeOut('slow');
					break;
				case 37:
					
					if( $dataIndex > 1 ) {
						$dataIndex--;
					} else {
						$dataIndex = postCount;
					}	
					var prev = container.find(".gallery-item" + "[data-index='" + $dataIndex + "']")
			        galleryImage = prev.find('.gallery-item-thumb').css("background-image");
			        galleryFull.css({ 'background-image' : galleryImage });
			        
			        break;
			    case 39:
			    
			    	if( $dataIndex < postCount ) {
						$dataIndex++;
					} else {
						$dataIndex = 1;
					}	
					var next = container.find(".gallery-item" + "[data-index='" + $dataIndex + "']")
			        galleryImage = next.find('.gallery-item-thumb').css("background-image");
			        galleryFull.css({ 'background-image' : galleryImage });
			    	
			    	break;
				default:
					return;
					
			} // close switch
		} // endif
	});	
	
	// if a nav arrow is clicked instead of an arrow being pressed, advance or revert the modal image accordingly
	
	galPrev.on('click', function(e){
		
		if(galleryModal.hasClass('modal-active')) {
		
			if( $dataIndex > 1 ) {
				$dataIndex--;
			} else {
				$dataIndex = postCount;
			}	
		
			var clickPrev = container.find(".gallery-item" + "[data-index='" + $dataIndex + "']")
			galleryImage = clickPrev.find('.gallery-item-thumb').css("background-image");
			galleryFull.css({ 'background-image' : galleryImage });
		
		}
	}); // close galPrev.on
	
	galNext.on('click', function(e){
		
		if(galleryModal.hasClass('modal-active')) {
		
			if( $dataIndex < postCount ) {
				$dataIndex++;
			} else {
				$dataIndex = 1;
			}	
		
			var clickNext = container.find(".gallery-item" + "[data-index='" + $dataIndex + "']")
			galleryImage = clickNext.find('.gallery-item-thumb').css("background-image");
			galleryFull.css({ 'background-image' : galleryImage });
		
		}
	}); // close galPrev.on
	
	
});