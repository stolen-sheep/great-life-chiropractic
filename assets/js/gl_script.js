jQuery(document).ready(function($){

	// variables

	var win 		 = $(window);
	var winW		 = $(window).width();
	var body		 = $("body");
	var toggle 		 = $("#menu-toggle");
	var menu   		 = $("#site-navigation");
	var page   		 = $("#page");
	var indicator	 = $("#menu-item-1000011");
	var cta			 = $("#cta");
	var content 	 = $("#content");
	var masthead	 = $("#masthead");
	var contentHeight;
	var mastHeight;
	var menuHeight;
	var currentPage;
	var currentTop;
	var pageBottom;


	// if the current page is not part of the primary navigation, set the currentPage var to the first item

	if ($(".current-menu-item").length !== 0) {
		currentPage  = $(".current-menu-item");
	} else {
		currentPage  = $(".menu-item").first();
	}

	var currentTop	 = currentPage.position().top;


	// add "js-enabled" class to body for javascript detection

	body.addClass("js-enabled");


	// update window width var on resize

	win.on('resize', function(){ winW = win.width(); });


	// on click, show menu and toggle page width

	toggle.on("click", function(){

		var $this = $(this);

		contentHeight = content.height();
		mastHeight	  = masthead.height();
		menuHeight	  = contentHeight + mastHeight;

		if ( body.hasClass("has-cta") ) {
			menu.css('bottom', pageBottom);
		}

		menu.toggleClass("menuOpen").css('min-height', menuHeight );
		page.toggleClass("menuOpen");
		$this.toggleClass("fa-bars fa-close");

	});


	// menu "lava-lamp" effect

	indicator.css("top", currentTop);

	$(".menu-item").on("mouseenter", function(){

		var $this = $(this);
		var top = $this.position().top;

		indicator.stop().animate({
			'top': top,

		}, 600, 'swing');

	});

	menu.on("mouseleave", function(){

		indicator.stop().delay(500).animate({
			'top': currentTop,

		}, 600, 'swing');

	});


// detect mobile device

	function checkMobileOS() {

    var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera;
    	if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) {
        return 'iOS';
    } else if (MobileUserAgent.match(/Android/i)) {
        return 'Android';
    } else {
	  return 'unknown';
    }
}

// if mobile device update href and attach link

var message_text = '';
var href = '';

if (checkMobileOS() == 'iOS') {
    href = "sms:6103488138&body=" + encodeURI(message_text);
}

if (checkMobileOS() == 'Android') {
    href = "sms:6103488138?body=" + encodeURI(message_text);
}
document.getElementById("sms_link").setAttribute('href', href);

}); // end doc.ready
