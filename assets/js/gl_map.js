document.addEventListener("DOMContentLoaded", function() {

var map;

function initMap() {
	var office = {lat: 40.055515, lng: -75.235517};
	map = new google.maps.Map(document.getElementById('google-map'), {
	  center: office,
	  zoom: 13,
	  styles: [{"elementType":"geometry","stylers":[{"color":"#cccccc"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#3f3e3e"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#eee"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#3f3e3e"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#3f3e3e"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#efefef"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#fff"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#4d4d59"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2c8ce6"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]}]	});
	
	// <img src="http://www.stolen-sheep.com/greatlife/wp-content/themes/greatlife/assets/img/logo_map.jpg" style=" width: 60px; display: block; margin: 0 auto;">
	
	var contentString = '<div id="content">' +
	'<h2 style="text-align:center;">Great Life Chiropractic</h2><h3 style="text-align:center;">7953 Ridge Avenue<br>Philadelphia, PA 19128</h3><a href="tel:2154836550" style="text-align:center; display: inline-block; width: 100%; margin: 0 auto; font-weight:bold;">215-483-6550</a><br><a href="mailto:greatLife.chiro@Hotmail.com" style="text-align:center; display: inline-block; width: 100%; margin: 0 auto; font-weight:bold;">GreatLife.Chiro@Hotmail.com</a>' +
	'</div>';
	
	var infowindow = new google.maps.InfoWindow({
    content: contentString
  	});
	var marker = new google.maps.Marker({
          position: office,
          map: map
    });
    marker.addListener('click', function() {
    	infowindow.open(map, marker);
  	});
}

initMap();

});