<?php
/**
 *	
 * Template Name: Standard Default Template
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GreatLife
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				
				<?php  get_template_part( 'template-parts/content', 'image-header' ); ?>
	
				<?php
				while ( have_posts() ) : the_post();
	
					get_template_part( 'template-parts/content', 'page' );
	
				endwhile; // End of the loop.
				?>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	
	<?php if( is_page( 'contact' ) ) {
		get_template_part( 'template-parts/content', 'map' );
	} ?>

	</div><!-- #content -->
</div><!-- #page -->

<?php
// if ACF Plugin is installed and user chooses to show a custom section, get that template part
	
	if (function_exists("get_field")) {
		
		if( get_field("show_team") == "yes" ) { get_template_part( 'template-parts/content', 'team' ); }
		
	}

get_template_part( 'template-parts/content', 'cta' );
get_footer();
