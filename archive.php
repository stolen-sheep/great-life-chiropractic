<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GreatLife
 */

get_header(); ?>
<div class="bs_container">
	<div class="row"> <?php // closes in sidebar.php ?>
		<div id="primary" class="content-area col-md-9">
			<main id="main" class="site-main" role="main">

			<?php

			$cat_title = single_cat_title("", false);
			$cat_object = get_category_by_slug($cat_title);
			$cat_ID = $cat_object->cat_ID;

			$args = array(
				'cat' => $cat_ID,
				'meta_key' => '_custom_post_order',
			  'orderby' => 'meta_value date',
				'order' => 'ASC'
			);

			$query = new WP_Query( $args );


			?>


			<?php
			if ( $query->have_posts() ) : ?>

				<?php
				/* Start the Loop */
				while ( $query->have_posts() ) : $query->the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;

			wp_reset_postdata();

			?>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php
get_sidebar();
get_template_part('template-parts/content', 'cta');
get_footer();
