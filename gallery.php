<?php
/*
*
* Template Name: Gallery
*
*/

get_header();

get_template_part( 'template-parts/content', 'image-header' );

get_template_part('template-parts/content', 'gallery');

get_template_part('template-parts/content', 'cta');

get_footer();

?>