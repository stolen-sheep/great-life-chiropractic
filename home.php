<?php
/**
 * The home template file
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package GreatLife
 */

get_header(); ?>


		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<?php get_template_part( 'template-parts/home/welcome', 'none' ); ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->
</div><!-- #page -->

<?php get_template_part( 'template-parts/home/schedule', 'none' ); ?>
<?php get_footer('home');
