<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GreatLife
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-md-3" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
	
	<?php // static "widgets" ?>
	<?php get_template_part( 'template-parts/content', 'social-links' ); ?>
	
	
</aside><!-- #secondary -->

</div> <?php // closes .row from archive.php ?>
</div>