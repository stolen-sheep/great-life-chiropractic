<?php
/**
 * The template for displaying the homepage footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GreatLife
 */

?>

<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info"></div><!-- .site-info -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
