<?php
/**
 * The header for our theme
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package GreatLife
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!--Font Awesome Icons-->
<script src="https://use.fontawesome.com/95aa0d9431.js"></script>
<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/gl_map.js"></script> -->
<!--Google Maps JS API-->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4_fmBeV4_WTDn0rgZrsFl1kZpmjXlm48&callback=initMap" async defer></script> -->
<!--Google Fonts: Open Sans, Vollkorn, Raleway-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Vollkorn:400,400i,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,700" rel="stylesheet">
<?php wp_head(); ?>
</head>

<?php if (function_exists("get_field") && get_field("show_cta") == "Yes" ) { ?>
	<body <?php body_class('has-cta'); ?>>
<?php } else { ?>
	<body <?php body_class(); ?>>
<?php } ?>

<i id="menu-toggle" class="fa fa-bars fa-2x"></i>

<nav id="site-navigation" class="main-navigation" role="navigation">
	<?php wp_nav_menu( 
			array( 
				'theme_location' => 'menu-1', 
				'menu_id' => 'primary-menu', 
				) 
			); 
	?>
	
<?php if( function_exists('get_field') && get_field('facebook_link', 'option') ) :
	
		$facebook_link    = get_field('facebook_link', 'option');
		$facebook_icon 	  = get_field('facebook_icon', 'option');
		$google_plus_link = get_field('google_plus_link', 'option');
		$google_plus_icon = get_field('google_plus_icon', 'option');
		$youtube_link 	  = get_field('youtube_link', 'option');
		$youtube_icon 	  = get_field('youtube_icon', 'option');
		$yelp_link		  = get_field('yelp_link', 'option');
		$yelp_icon		  = get_field('yelp_icon', 'option');
		
?>
		
<ul id="header_social-links">
		<li>
			<a href="<?php echo $yelp_link; ?>" target="_blank">
				<i class="fa <?php echo 'fa-' . $yelp_icon; ?> fa-lg"></i>
			</a>
		</li>
		<li>
			<a href="<?php echo $youtube_link; ?>" target="_blank">
				<i class="fa <?php echo 'fa-' . $youtube_icon; ?> fa-lg"></i>
			</a>
		</li>
		<li>
			<a href="<?php echo $google_plus_link; ?>" target="_blank">
				<i class="fa <?php echo 'fa-' . $google_plus_icon; ?> fa-lg"></i>
			</a>
		</li>
		<li>
			<a href="<?php echo $facebook_link; ?>" target="_blank">
				<i class="fa <?php echo 'fa-' . $facebook_icon; ?> fa-lg"></i>
			</a>
		</li>
</ul>

<?php endif; ?>
		


</nav><!-- #site-navigation -->

<div id="page" class="site">
	
	<?php if( is_home() || is_front_page() ) { get_template_part( 'template-parts/home/slider', 'none' ); } ?>
	
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'greatlife' ); ?></a>
	
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">

				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?><img id="logoMark" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png"></a></h1>

			<?php $blogdesc = get_bloginfo('description', 'display');
			if ( $blogdesc || is_customize_preview() ) : 
			
				// replace commas in blog description with <br> line breaks		
				
				$blogdesc = preg_replace("/, */", "<br> ", $blogdesc); ?>
			
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><p class="site-description"><?php echo $blogdesc; /* WPCS: xss ok. */ ?></p></a>
			<?php
			endif; ?>
			
		</div><!-- .site-branding -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
